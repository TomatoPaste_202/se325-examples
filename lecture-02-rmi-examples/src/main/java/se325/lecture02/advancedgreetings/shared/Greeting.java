package se325.lecture02.advancedgreetings.shared;

import java.io.Serializable;

public class Greeting implements Serializable {

    private String message;

    public Greeting(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
