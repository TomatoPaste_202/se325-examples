package se325.lecture02.advancedgreetings.server;

import se325.lecture02.advancedgreetings.shared.Greeting;
import se325.lecture02.advancedgreetings.shared.GreetingService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class EnglishGreetingServiceServant extends UnicastRemoteObject implements GreetingService {

    public EnglishGreetingServiceServant() throws RemoteException {
    }

    @Override
    public Greeting getGreeting(String name) throws RemoteException {
        return new Greeting("Hello, " + name + "!");
    }
}
