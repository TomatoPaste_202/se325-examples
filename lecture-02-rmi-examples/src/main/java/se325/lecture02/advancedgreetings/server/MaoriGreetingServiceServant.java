package se325.lecture02.advancedgreetings.server;

import se325.lecture02.advancedgreetings.shared.Greeting;
import se325.lecture02.advancedgreetings.shared.GreetingService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class MaoriGreetingServiceServant extends UnicastRemoteObject implements GreetingService {

    public MaoriGreetingServiceServant() throws RemoteException {
    }

    @Override
    public Greeting getGreeting(String name) throws RemoteException {
        return new Greeting("Kia Ora, " + name + "!");
    }
}
