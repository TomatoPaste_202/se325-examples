package se325.lecture05.jacksonsamples.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/helloJacksonServices")
public class HelloJacksonApplication extends Application {

    private Set<Object> singletons = new HashSet<>();

    public HelloJacksonApplication() {
        singletons.add(new HelloJacksonResource());
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
